package cn.piflow.local

import cn.piflow.util.Logging
import org.quartz.{JobDetail, JobKey, SchedulerException, SchedulerListener, Trigger, TriggerKey}

/**
	* @author bluejoe2008@gmail.com
	*/
class QuartzSchedulerListenerImpl(table: QuartzTriggerTable) extends SchedulerListener with Logging {
	def jobScheduled(trigger: Trigger) {
		logger.debug(String.format("job scheduled: %s", trigger.getKey.getName));
	}

	def jobUnscheduled(triggerKey: TriggerKey) {
		logger.debug(String.format("job unscheduled: %s", triggerKey.getName));
	}

	def triggerFinalized(trigger: Trigger) {
		table.get(trigger.getKey).notifyFinalized();
		logger.debug(String.format("job finalized: %s", trigger.getKey.getName));

		//do not logout: historic records
		//table.logout(trigger.getKey);
	}

	def triggerPaused(triggerKey: TriggerKey) {}

	def triggersPaused(triggerGroup: String) {}

	def triggerResumed(triggerKey: TriggerKey) {}

	def triggersResumed(triggerGroup: String) {}

	def jobAdded(jobDetail: JobDetail) {}

	def jobDeleted(jobKey: JobKey) {}

	def jobPaused(jobKey: JobKey) {}

	def jobsPaused(jobGroup: String) {}

	def jobResumed(jobKey: JobKey) {}

	def jobsResumed(jobGroup: String) {}

	def schedulerError(msg: String, cause: SchedulerException) {}

	def schedulerInStandbyMode() {}

	def schedulerStarted() {}

	def schedulerStarting() {}

	def schedulerShutdown() {}

	def schedulerShuttingdown() {}

	def schedulingDataCleared() {}
}