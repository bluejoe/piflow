package cn.piflow.flow

import cn.piflow.FlowEngine
import cn.piflow.rpc.FlowEngineServer
import org.junit.{Assert, Test}

class RpcTest {
	@Test
	def test1() {
		FlowEngineServer.main(Array("1224", "/rpc"));
		val fe = FlowEngine.connect("http://localhost:1224/rpc");
		val res1 = fe.getEchoService().toString()
		val res2 = fe.getEchoService().echo(1, 2, 3)
		Assert.assertEquals(Seq(1, 2, 3), res2);
	}
}

