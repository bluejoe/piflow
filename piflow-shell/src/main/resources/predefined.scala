import java.util._

import cn.piflow._
import cn.piflow.dsl._
import cn.piflow.io._
import cn.piflow.processor.ds._
import cn.piflow.shell._
import org.apache.spark.sql._

import scala.sys.process._;

class PRELOAD_CODES()(implicit val spark: SparkSession, implicit val engine: FlowEngine) {

	import spark.implicits._;

	def THIS_METHOD_WILL_NEVER_BE_CALLED_ONLY_FOR_SYNTAX_CHECK() {
		"ls" !;
		println(engine);
		new Date();
		val pl = SeqAsSource(1 to 1000) > DoMap[Int, Int](_ + 1) > ConsoleSink();
		asGraph(pl);
		toRunnable(pl);
		pl !;
		pl !@ (Start.now)
	}
}