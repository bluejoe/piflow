package cn.piflow

import cn.piflow.dsl._

package object shell {
	implicit def toRunnable(flowGraph: FlowGraph)(implicit engine: FlowEngine): RunnableFlowGraph = {
		new RunnableFlowGraph(flowGraph);
	}

	implicit def toRunnable(chain: ChainWithTail[_])(implicit engine: FlowEngine): RunnableFlowGraph = {
		new RunnableFlowGraph(asGraph(chain));
	}
}