package cn.piflow.shell

import java.util.Date

import cn.piflow._
import cn.piflow.dsl._
import cn.piflow.util.FormatUtils

/**
	* @author bluejoe2008@gmail.com
	*/
class RunnableFlowGraph(flowGraph: FlowGraph)(implicit engine: FlowEngine) {
	def this(node: ChainWithTail[_])(implicit engine: FlowEngine) =
		this(asGraph(node))(engine);

	def !() {
		val time1 = +System.currentTimeMillis();
		val job = engine.run(flowGraph);
		val time2 = System.currentTimeMillis();
		val jobId = job.getId();
		val cost = time2 - time1;
		println(s"job complete: id=$jobId, time cost=${cost}ms");
	}

	def &() {
		val job = engine.schedule(flowGraph);
		val jobId = job.getId();
		println(s"job scheduled: id=$jobId");
	}

	def !@(date: Date) = {
		val job = engine.schedule(flowGraph, Start.at(date));
		printScheduledJobInfo(job);
	}

	def !@(start: Start.Builder = Start.now, repeat: Repeat.Builder = Repeat.once) = {
		val job = engine.schedule(flowGraph, start, repeat);
		printScheduledJobInfo(job);
	}

	def !@(cronExpression: String) = {
		val job = engine.schedule(flowGraph, Start.now, Repeat.cronedly(cronExpression));
		printScheduledJobInfo(job);
	}

	private def printScheduledJobInfo(job: ScheduledJob) = {
		val jobId = job.getId();
		val nftime = FormatUtils.format(job.getNextFireTime());
		println(s"job scheduled: id=$jobId, next fired time=$nftime");
	}

	def !@(delay: Long) = {
		val job = engine.schedule(flowGraph, Start.later(delay));
		printScheduledJobInfo(job);
	}
}